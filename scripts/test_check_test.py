# SPDX-FileCopyrightText: <text>Copyright 2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

import pytest
import json
import logging
import os
import gitlab
from typing import Set
from pathlib import Path
from test_check import get_expected_test_ids, \
                       get_success_test_ids, create_logger, \
                       fetch_test_reports


def test_create_logger():
    logger = create_logger("test_logger")
    assert logger.name == "test_logger"
    assert len(logger.handlers) == 2
    assert logger.handlers[0].level == logging.INFO
    assert logger.handlers[1].level == logging.ERROR


class Report:
    def __init__(self):
        self.test_suites = [
            {
                "name": "suite1",
                "test_cases": [
                    {"name": "test1", "status": "success"},
                    {"name": "test2", "status": "failed"}
                ]
            },
            {
                "name": "suite2",
                "test_cases": [
                    {"name": "test3", "status": "success"}
                ]
            }
        ]


sample_test_report = Report()


def test_get_success_test_ids():
    expected = {"suite1.test1", "suite2.test3"}
    result = get_success_test_ids(sample_test_report)
    assert result == expected


def test_parsing_json_file_with_multiple_suites_returns_pairs_correctly(
        tmp_path: Path) -> None:

    test_data = {
        "suite1": ["test1", "test2"],
        "suite2": ["test3"]
    }
    json_file_path = tmp_path / "test_data.json"
    with open(json_file_path, 'w', encoding='utf-8') as f:
        json.dump(test_data, f)

    result = get_expected_test_ids(str(json_file_path))
    expected = {"suite1.test1", "suite1.test2", "suite2.test3"}

    assert result == expected


def test_parsing_json_file_with_empty_test_suite_returns_no_test(
        tmp_path: Path) -> None:

    test_data = {
        "suite1": [],
    }
    json_file_path = tmp_path / "test_data.json"
    with open(json_file_path, 'w', encoding='utf-8') as f:
        json.dump(test_data, f)

    result = get_expected_test_ids(str(json_file_path))
    expected = set()

    assert result == expected


def test_report_can_be_read() -> None:
    api_token = os.getenv('CI_BOT_API_TOKEN')
    project_id = os.getenv('CI_PROJECT_ID')
    url = f"{os.getenv('CI_SERVER_PROTOCOL')}://{os.getenv('CI_SERVER_HOST')}"
    gl = gitlab.Gitlab(url, private_token=api_token)
    project = gl.projects.get(project_id)
    pipeline_schedule = project.pipelineschedules.list()[0]
    pipeline_list = pipeline_schedule.pipelines.list(get_all=True)
    successful_pipeline_list = [
        pipeline for pipeline in pipeline_list
        if pipeline.status == "success"
        ]
    pipeline_id = successful_pipeline_list[-1].id
    test_reports = fetch_test_reports(
            url, api_token, pipeline_id, project_id)
    assert test_reports[0].test_suites[0]["test_cases"][0]["status"]


if __name__ == "__main__":
    pytest.main()
